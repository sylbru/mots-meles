port module Main exposing (..)

import Array exposing (Array)
import Browser
import Codec exposing (Codec)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Http
import Json.Decode exposing (Decoder)
import Json.Encode
import Svg exposing (Svg)
import Svg.Attributes as SvgAttr


type Msg
    = ClickedGeneratePuzzle
    | TypedMysteryWord String
    | GotNewPuzzle Grid (List Word)
    | CheckedShowWords Bool
    | ClickedLoadPuzzle Puzzle
    | ClickedSavePuzzle
    | ClickedDeletePuzzle Grid
    | NoOp


type alias Model =
    { grid : Grid
    , words : List Word
    , mysteryWord : String
    , pen : PenType
    , showWords : Bool
    , savedPuzzles : List Puzzle
    }


type alias Puzzle =
    ( Grid, List Word )


type alias Word =
    ( String, Position, Direction )


type alias Grid =
    Array (Array Char)


type alias Position =
    { x : Int, y : Int }


type Direction
    = Right
    | Left
    | Down
    | Up
    | UpRight
    | DownRight
    | UpLeft
    | DownLeft


type PenType
    = BallPen
    | Highlighter


type alias Flags =
    Json.Encode.Value


type alias SavedData =
    { savedPuzzles : List Puzzle }


emptyGrid : Grid
emptyGrid =
    Array.fromList
        []


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { grid = emptyGrid
      , words = []
      , mysteryWord = ""
      , pen = Highlighter
      , showWords = False
      , savedPuzzles = []
      }
        |> loadSavedData flags
    , Cmd.none
    )


loadSavedData : Flags -> Model -> Model
loadSavedData flags model =
    let
        flagsDecoder =
            Codec.decoder (Codec.list puzzleCodec)
    in
    case Json.Decode.decodeValue flagsDecoder flags of
        Ok savedData ->
            { model | savedPuzzles = savedData }

        Err _ ->
            model


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickedGeneratePuzzle ->
            ( model, requestGenerateGrid model.mysteryWord )

        TypedMysteryWord mysteryWord ->
            ( { model | mysteryWord = mysteryWord }, Cmd.none )

        GotNewPuzzle grid words ->
            ( { model | grid = grid, words = words }, Cmd.none )

        CheckedShowWords checked ->
            ( { model | showWords = checked }, Cmd.none )

        ClickedLoadPuzzle ( grid, words ) ->
            ( { model | grid = grid, words = words }, Cmd.none )

        ClickedSavePuzzle ->
            let
                newSavedGrids =
                    ( model.grid, model.words ) :: model.savedPuzzles
            in
            ( { model | savedPuzzles = newSavedGrids }, save newSavedGrids )

        ClickedDeletePuzzle grid ->
            let
                newSavedGrids =
                    model.savedPuzzles
                        |> List.filter (\( grid_, _ ) -> grid_ /= grid)
            in
            ( { model | savedPuzzles = newSavedGrids }
            , save newSavedGrids
            )

        NoOp ->
            ( model, Cmd.none )


puzzleCodec : Codec Puzzle
puzzleCodec =
    let
        gridCodec : Codec Grid
        gridCodec =
            Codec.array (Codec.array Codec.char)

        wordCodec : Codec Word
        wordCodec =
            Codec.triple Codec.string positionCodec directionCodec

        positionCodec : Codec Position
        positionCodec =
            Codec.object (\x y -> { x = x, y = y })
                |> Codec.field "x" .x Codec.int
                |> Codec.field "y" .y Codec.int
                |> Codec.buildObject

        directionCodec : Codec Direction
        directionCodec =
            Codec.enum Codec.string
                [ ( "Right", Right )
                , ( "Left", Left )
                , ( "Down", Down )
                , ( "Up", Up )
                , ( "UpRight", UpRight )
                , ( "DownRight", DownRight )
                , ( "UpLeft", UpLeft )
                , ( "DownLeft", DownLeft )
                ]
    in
    Codec.tuple gridCodec (Codec.list wordCodec)


save : List Puzzle -> Cmd Msg
save savedPuzzles =
    saveToLocalStorage (Codec.encodeToValue (Codec.list puzzleCodec) savedPuzzles)


requestGenerateGrid : String -> Cmd Msg
requestGenerateGrid mysteryWord =
    let
        gridDecoder : Decoder Grid
        gridDecoder =
            Json.Decode.list
                (Json.Decode.string
                    |> Json.Decode.map (\row -> String.toList row |> Array.fromList)
                )
                |> Json.Decode.map Array.fromList

        positionDecoder : Decoder Position
        positionDecoder =
            Json.Decode.map2
                (\x y -> { x = x, y = y })
                (Json.Decode.field "x" Json.Decode.int)
                (Json.Decode.field "y" Json.Decode.int)

        directionDecoder : Decoder Direction
        directionDecoder =
            Json.Decode.string
                |> Json.Decode.andThen
                    (\direction ->
                        case direction of
                            "Right" ->
                                Json.Decode.succeed Right

                            "Left" ->
                                Json.Decode.succeed Left

                            "Down" ->
                                Json.Decode.succeed Down

                            "Up" ->
                                Json.Decode.succeed Up

                            "UpRight" ->
                                Json.Decode.succeed UpRight

                            "DownRight" ->
                                Json.Decode.succeed DownRight

                            "UpLeft" ->
                                Json.Decode.succeed UpLeft

                            "DownLeft" ->
                                Json.Decode.succeed DownLeft

                            _ ->
                                Json.Decode.fail "Invalid direction"
                    )

        placedWordDecoder : Decoder Word
        placedWordDecoder =
            Json.Decode.map3
                (\word position direction -> ( word, position, direction ))
                (Json.Decode.index 0 Json.Decode.string)
                (Json.Decode.index 1 positionDecoder)
                (Json.Decode.index 2 directionDecoder)

        decoder : Decoder { grid : Grid, words : List Word }
        decoder =
            Json.Decode.map2
                (\grid words -> { grid = grid, words = words })
                (Json.Decode.field "grid" gridDecoder)
                (Json.Decode.field "words" (Json.Decode.list placedWordDecoder))

        onResult : Result Http.Error { grid : Grid, words : List Word } -> Msg
        onResult result =
            case result of
                Ok { grid, words } ->
                    GotNewPuzzle grid words

                Err error ->
                    NoOp
    in
    Http.get { url = "/generate/" ++ mysteryWord, expect = Http.expectJson onResult decoder }


view : Model -> Html Msg
view model =
    Html.div [ Attributes.attribute "data-printable" "" ]
        [ Html.h1
            [ Attributes.style "font-size" "1.5em"
            , Attributes.style "text-transform" "uppercase"
            ]
            [ Html.text "Mots "
            , Html.span
                [ Attributes.style "display" "inline-block"
                , Attributes.style "animation" "mix infinite 20s ease"
                , Attributes.style "color" "#444"
                ]
                [ Html.text "mixés" ]
            ]
        , Html.p []
            [ Html.input
                [ Attributes.value model.mysteryWord
                , Attributes.placeholder "(mot mystère)"
                , Attributes.style "text-transform" "uppercase"
                , Events.onInput TypedMysteryWord
                ]
                []
            , Html.button [ Events.onClick ClickedGeneratePuzzle ] [ Html.text "Générer une grille" ]
            ]
        , Html.div
            [ Attributes.attribute "data-printable" ""
            , Attributes.class "printable-container"
            ]
            [ viewGrid model.grid model.words model.pen model.showWords
            , viewWords model.words
            ]
        , Html.p []
            [ Html.label []
                [ Html.input [ Attributes.type_ "checkbox", Events.onCheck CheckedShowWords, Attributes.checked model.showWords ] []
                , Html.text " Montrer les mots dans la grille"
                ]
            ]
        , viewSavedPuzzles model.savedPuzzles
        ]


viewGrid : Grid -> List Word -> PenType -> Bool -> Html Msg
viewGrid grid words pen showWords =
    let
        viewLetters : Grid -> Svg Msg
        viewLetters grid_ =
            grid_
                |> Array.toList
                |> List.indexedMap viewRow
                |> Svg.g []

        viewRow yOffset row =
            Svg.g []
                (Array.toList row
                    |> List.indexedMap
                        (\i char ->
                            Svg.text_
                                [ SvgAttr.x (String.fromFloat (toFloat i + 0.5))
                                , SvgAttr.y (String.fromFloat (toFloat yOffset + 0.5))
                                , SvgAttr.textAnchor "middle"
                                , SvgAttr.dominantBaseline "middle"
                                , SvgAttr.fontSize ".65"
                                ]
                                [ Svg.text <| String.fromChar char ]
                        )
                )

        viewMarkedWords : List Word -> Svg Msg
        viewMarkedWords words_ =
            words_
                |> List.map
                    (\( word, position, direction ) ->
                        let
                            length =
                                String.length word - 1

                            lineTo =
                                case direction of
                                    Right ->
                                        "h" ++ String.fromInt length

                                    Left ->
                                        "h-" ++ String.fromInt length

                                    Down ->
                                        "v" ++ String.fromInt length

                                    Up ->
                                        "v-" ++ String.fromInt length

                                    UpRight ->
                                        "l" ++ String.fromInt length ++ ",-" ++ String.fromInt length

                                    DownRight ->
                                        "l" ++ String.fromInt length ++ "," ++ String.fromInt length

                                    UpLeft ->
                                        "l-" ++ String.fromInt length ++ ",-" ++ String.fromInt length

                                    DownLeft ->
                                        "l-" ++ String.fromInt length ++ "," ++ String.fromInt length

                            pathData =
                                [ "M"
                                    ++ String.fromFloat (toFloat position.x + 0.5)
                                    ++ ","
                                    ++ String.fromFloat (toFloat position.y + 0.5)
                                , lineTo
                                ]
                                    |> String.join " "

                            penAttributes =
                                case pen of
                                    BallPen ->
                                        [ SvgAttr.strokeWidth "0.1"
                                        , SvgAttr.strokeLinecap "round"
                                        , SvgAttr.stroke "#2f4db6"
                                        ]

                                    Highlighter ->
                                        [ SvgAttr.strokeWidth ".65"
                                        , SvgAttr.strokeLinecap "square"
                                        , SvgAttr.stroke "#fff91780"
                                        ]
                        in
                        Svg.path
                            ([ SvgAttr.d pathData ] ++ penAttributes)
                            []
                    )
                |> Svg.g []

        height : Int
        height =
            Array.length grid

        width : Int
        width =
            Array.get 0 grid
                |> Maybe.map Array.length
                |> Maybe.withDefault 0
    in
    Html.div [ Attributes.class "grid" ]
        [ Svg.svg
            [ SvgAttr.viewBox <| "0 0 " ++ String.fromInt width ++ " " ++ String.fromInt height
            ]
            [ if showWords then
                viewMarkedWords words

              else
                Svg.text ""
            , viewLetters grid
            ]
        ]


viewWords : List Word -> Html Msg
viewWords words =
    let
        viewWord : Word -> Html Msg
        viewWord ( word, _, _ ) =
            Html.li [] [ Html.text word ]

        minimalColumnWidth : Int
        minimalColumnWidth =
            List.map (\( word, _, _ ) -> String.length word) words
                |> List.maximum
                |> Maybe.withDefault 0
                |> (+) 1
    in
    Html.ul
        [ Attributes.class "words"
        , Attributes.style
            "grid-template-columns"
            ("repeat(auto-fit, minmax(" ++ String.fromInt minimalColumnWidth ++ "ch, 1fr))")
        ]
        (List.map viewWord words)


viewSavedPuzzles : List Puzzle -> Html Msg
viewSavedPuzzles savedPuzzles =
    let
        viewSavedPuzzle : Puzzle -> Html Msg
        viewSavedPuzzle ( grid, words ) =
            Html.li []
                [ Html.text <| gridToString grid ++ " "
                , Html.button [ Events.onClick (ClickedLoadPuzzle ( grid, words )) ] [ Html.text "Charger" ]
                , Html.text " "
                , Html.button [ Events.onClick (ClickedDeletePuzzle grid) ] [ Html.text "Supprimer" ]
                ]
    in
    Html.section []
        [ Html.h2 [] [ Html.text "Grilles enregistrées" ]
        , Html.p [] [ Html.button [ Events.onClick ClickedSavePuzzle ] [ Html.text "Enregistrer la grille" ] ]
        , Html.ul [] (List.map viewSavedPuzzle savedPuzzles)
        ]


gridToString : Grid -> String
gridToString grid =
    grid
        |> Array.toList
        |> List.map
            (\row ->
                row
                    |> Array.toList
                    |> List.map String.fromChar
                    |> String.join ""
            )
        |> String.join "\n"


port saveToLocalStorage : Json.Encode.Value -> Cmd msg


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }
