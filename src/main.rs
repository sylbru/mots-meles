use actix_files::Files;
use actix_files::NamedFile;
use actix_web::{get, App, HttpRequest, HttpResponse, HttpServer, Responder};
use itertools::iproduct;
use rand::prelude::*;
use serde::Serialize;
use std::fs;
use std::ops::Range;

// TODO
// - custom dictionary : add words but also prioritize known words
// - prevent a placed word from sharing too many letters with another placed word
// - expand ligatures (cœur -> coeur)
//
// - make it able to create bigger grids
//   - log things to determine where it tries something too many times,
//     and put a maximum number of tries after which it must give up
//   - will probably need to prioritize words/positions sharing letters
//      (but some words already are sharing too much letters)
//      (maybe try anyway, just prioritize words and positions sharing one letter, especially at the beginning)
//   - and/or prioritize long words first?
//   - try remove [??]
//   - better perf (make it cheap to try a lot of words)
//   - will we need to remove diacritics? it’s a pretty cool change to keep them if possible
//     (although it seems to make the game too easy sometimes)
// - default grid sizes: 12×16, 24×18, 12×38, 24×38
//   mystery word should have a maximum length depending on the size of the grid
//   (apparently small grids always have a 6- or 7-letter word)
// - UI: when hovering a word, highlight this word only in the grid
// - make into a module
// - test: write unit tests
// - test: write fuzz tests
// - perf: don’t recompute everything every time:
//   - cache filtered dictionary
//   - memoize all_valid_positions_and_directions
// - refactor: turn functions into methods
// - refactor: use codec in requestGenerateGrid (need to adapt some places to use { grid, words } instead of (grid, words))
// - bonus goals:
//   - prevent a word from appearing (by mistake) in several places
//     (probably just by brutechecking all positions for each word)
//   - require each word to have at least one letter that isn’t used by another word,
//     so that you have to mark that word in order to complete the grid (option "no-useless-words")
//   - prevent valid dictionary words from appearing by mistake
//     (probably just by brutechecking all positions for each word)
//   - add an option to randomize the order of the mystery word letters
// - UI: make it usable (touch/click events for marking words)
// - CLI interface with options

type Grid = Vec<Vec<char>>;

#[derive(Debug, Serialize, Clone)]
struct Position {
    x: usize,
    y: usize,
}

type Dictionary = Vec<String>;

#[derive(Debug, Serialize, Copy, Clone)]
enum Direction {
    Right,
    Left,
    Down,
    Up,
    UpRight,
    DownRight,
    UpLeft,
    DownLeft,
}

const EMPTY: char = '.';

#[derive(Serialize)]
struct GenerateOutput {
    grid: Vec<String>,
    width: u8,
    height: u8,
    mystery_word: String,
    words: Vec<(String, Position, Direction)>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(home)
            .service(generate)
            .service(Files::new("/build", "frontend/build"))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

#[get("/")]
async fn home() -> impl Responder {
    NamedFile::open_async("public/index.html").await
}

#[get("/generate/{mystery_word}")]
async fn generate(req: HttpRequest) -> impl Responder {
    let mystery_word = req.match_info().get("mystery_word").unwrap().to_uppercase();
    let width = 16;
    let height = 16;
    println!(
        "Generating {}x{} grid with mystery word: {}",
        width, height, &mystery_word
    );
    let (grid, mut words) = generate_grid(width, height, &mystery_word);
    let serialized_grid = grid
        .iter()
        .map(|row| row.iter().cloned().collect::<String>())
        .collect::<Vec<String>>();

    let mut rng = rand::thread_rng();
    words.shuffle(&mut rng);

    let output = GenerateOutput {
        grid: serialized_grid,
        width,
        height,
        mystery_word: mystery_word.to_string(),
        words,
    };

    HttpResponse::Ok().json(output)
}

fn generate_grid(
    width: u8,
    height: u8,
    mystery_word: &str,
) -> (Grid, Vec<(String, Position, Direction)>) {
    let seed = rand::random::<u64>();
    println!("Seed used: {}", seed);
    let mut rng = rand::rngs::StdRng::seed_from_u64(seed);
    let mut grid = make_empty_grid(width, height);
    let dictionary = read_dictionary("dictionaries/liste_francais.txt");
    let custom_dictionary = read_dictionary("dictionaries/custom_dictionary.txt");

    loop {
        let (success, placed_words) = fill_grid(mystery_word, &mut grid, &dictionary, &custom_dictionary, &mut rng);

        if success {
            return (grid, placed_words);
        } else {
            grid = make_empty_grid(width, height);
        }
    }
}

fn fill_grid(
    mystery_word: &str,
    grid: &mut Grid,
    dictionary: &Dictionary,
    custom_dictionary: &Dictionary,
    rng: &mut rand::rngs::StdRng,
) -> (bool, Vec<(String, Position, Direction)>) {
    let mut empty_cells = grid[0].len() * grid.len();
    let mut placed_words = Vec::new();
    let mut attempted_words = Vec::new();

    while empty_cells > mystery_word.len() {
        // Pick a word
        let random_word = normalize_word(
            if placed_words.len() < 2 && placed_words.len() < custom_dictionary.len() {
                custom_dictionary.choose(rng).unwrap()
            } else {
                dictionary.choose(rng).unwrap()
            }
        );
        let available_empty_cells =
            empty_cells.saturating_sub(mystery_word.chars().collect::<Vec<char>>().len());

        // skip if word has already been placed or tried
        if attempted_words.iter().any(|w| w == &random_word)
            || placed_words
                .iter()
                .any(|w: &(String, Position, Direction)| {
                    random_word.starts_with(&w.0) || w.0.starts_with(&random_word)
                })
        {
            continue;
        }

        attempted_words.push(random_word.clone());

        match attempt_place_word(&random_word, grid, available_empty_cells, rng) {
            Some((position, direction)) => {
                empty_cells = count_empty_cells(grid);
                placed_words.push((random_word.to_string(), position, direction));
            }
            None => {
                if attempted_words.len() == dictionary.len() {
                    println!("All words tried, can’t complete grid");
                    return (false, Vec::new());
                }
            }
        }
    }

    fill_empty_cells(mystery_word, grid);

    println!("{} words placed: {:?}", placed_words.len(), placed_words);
    return (true, placed_words);
}

fn normalize_word(word: &String) -> String {
    word.to_uppercase()
        .replace("Œ", "OE")
        .replace("Œ", "AE")
}

fn count_empty_cells(grid: &Grid) -> usize {
    let mut counter = 0;

    for row in grid {
        for cell in row {
            if *cell == EMPTY {
                counter += 1;
            }
        }
    }

    counter
}

fn fill_empty_cells(mystery_word: &str, grid: &mut Grid) {
    let mut i_mystery_letter = 0;
    for row in grid {
        for cell in row {
            if *cell == EMPTY {
                *cell = mystery_word.chars().nth(i_mystery_letter).unwrap();
                i_mystery_letter += 1;
            }
        }
    }
}

fn attempt_place_word(
    word: &str,
    grid: &mut Grid,
    available_empty_cells: usize,
    rng: &mut rand::rngs::StdRng,
) -> Option<(Position, Direction)> {
    let mut all_positions_directions = all_valid_positions_and_directions(word.len(), grid);
    all_positions_directions.shuffle(rng);

    for (position, direction) in all_positions_directions {
        if let Some(result) = can_add_word(word, &position, &direction, grid, available_empty_cells)
        {
            // result contains the positions and characters to change
            add_chars(result, grid);

            // Word placed, we can stop here
            return Some((position, direction));
        }
    }

    // No available position for this word
    return None;
}

fn positions_in_area_with_direction(
    x_range: Range<usize>,
    y_range: Range<usize>,
    direction: Direction,
) -> Vec<(Position, Direction)> {
    iproduct!(x_range, y_range)
        .map(|coords| {
            (
                Position {
                    x: coords.0,
                    y: coords.1,
                },
                direction,
            )
        })
        .collect()
}

fn all_valid_positions_and_directions(
    word_length: usize,
    grid: &Grid,
) -> Vec<(Position, Direction)> {
    let mut all_positions_directions = Vec::new();
    let width = grid[0].len();
    let height = grid.len();

    if word_length <= width {
        all_positions_directions.append(&mut positions_in_area_with_direction(
            0..width - word_length + 1,
            0..height,
            Direction::Right,
        ));
        all_positions_directions.append(&mut positions_in_area_with_direction(
            width - word_length + 1..width,
            0..height,
            Direction::Left,
        ));
    }

    if word_length <= height {
        all_positions_directions.append(&mut positions_in_area_with_direction(
            0..width,
            0..height - word_length + 1,
            Direction::Down,
        ));
        all_positions_directions.append(&mut positions_in_area_with_direction(
            0..width,
            (height - word_length + 1)..height,
            Direction::Up,
        ));
    }

    if word_length <= width && word_length <= height {
        all_positions_directions.append(&mut positions_in_area_with_direction(
            0..width - word_length + 1,
            0..height - word_length + 1,
            Direction::DownRight,
        ));
        all_positions_directions.append(&mut positions_in_area_with_direction(
            width - word_length + 1..width,
            0..height - word_length + 1,
            Direction::DownLeft,
        ));
        all_positions_directions.append(&mut positions_in_area_with_direction(
            0..width - word_length + 1,
            (height - word_length + 1)..height,
            Direction::UpRight,
        ));
        all_positions_directions.append(&mut positions_in_area_with_direction(
            width - word_length + 1..width,
            (height - word_length + 1)..height,
            Direction::UpLeft,
        ));
    }

    all_positions_directions
}

fn read_dictionary(path: &str) -> Dictionary {
    let file = fs::read_to_string(path);

    match file {
        Ok(actual_file) => actual_file
            .lines()
            .filter(|word| {
                word.chars().collect::<Vec<char>>().len() >= 4
                    && !word.contains(" ")
                    && !word.contains("'")
                    && !word.contains("-")
                    && !word.contains(".")
            })
            .map(|word| word.to_owned())
            .collect(),
        Err(_) => vec![],
    }
}

fn make_empty_grid(width: u8, height: u8) -> Grid {
    let grid = vec![vec![EMPTY; width.into()]; height.into()];

    grid
}

fn add_chars(chars: Vec<(Position, char)>, grid: &mut Grid) -> () {
    for (position, c) in chars {
        grid[position.y][position.x] = c;
    }
}

fn move_by(position: &Position, direction: &Direction) -> Position {
    match direction {
        Direction::Right => Position {
            x: position.x + 1,
            y: position.y,
        },
        Direction::Left => Position {
            x: position.x.overflowing_sub(1).0,
            y: position.y,
        },
        Direction::Down => Position {
            x: position.x,
            y: position.y + 1,
        },
        Direction::Up => Position {
            x: position.x,
            y: position.y.overflowing_sub(1).0,
        },
        Direction::UpRight => Position {
            x: position.x + 1,
            y: position.y.overflowing_sub(1).0,
        },
        Direction::DownRight => Position {
            x: position.x + 1,
            y: position.y + 1,
        },
        Direction::UpLeft => Position {
            x: position.x.overflowing_sub(1).0,
            y: position.y.overflowing_sub(1).0,
        },
        Direction::DownLeft => Position {
            x: position.x.overflowing_sub(1).0,
            y: position.y + 1,
        },
    }
}

fn can_add_word(
    word: &str,
    from: &Position,
    direction: &Direction,
    grid: &Grid,
    available_empty_cells: usize,
) -> Option<Vec<(Position, char)>> {
    let mut position = from.clone();
    let mut used_empty_cells = 0;
    let mut chars_and_positions = Vec::new();

    for c in word.to_uppercase().chars() {
        let out_of_bounds = position.y > grid.len() - 1 || position.x > grid[0].len() - 1;

        if out_of_bounds || used_empty_cells >= available_empty_cells {
            return None;
        } else {
            let empty_cell = grid[position.y][position.x] == EMPTY;
            let compatible_cell = grid[position.y][position.x] == c;

            if !empty_cell && !compatible_cell {
                return None;
            }
        }

        if grid[position.y][position.x] == EMPTY {
            used_empty_cells += 1;
        }

        chars_and_positions.push((position.clone(), c));
        position = move_by(&position, &direction);
    }

    return Some(chars_and_positions);
}

#[allow(dead_code)]
fn print_grid(grid: &Grid) {
    for row in grid {
        for cell in row {
            print!("{} ", cell);
        }
        println!();
    }

    println!();
}
